### Build and push to Gitlab container registry
```bash
$ docker login registry.gitlab.com -u (username) -p (token)
$ docker build --file Dockerfile.production -t registry.gitlab.com/(group-name)/(project-name)/(image-name):latest .
$ docker push registry.gitlab.com/(group-name)/(project-name)/(image-name):latest
```
